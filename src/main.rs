use binoxxo_solver::{Field, FieldState};
use gtk::prelude::*;
use gtk::{
    Button, ButtonExt, Inhibit, Label, LabelExt,
    Orientation::{Horizontal, Vertical},
    StateFlags, WidgetExt, Window, WindowType,
};
use relm::{connect, Relm, Update, UpdateNew, Widget};
use relm_derive::Msg;

const MIN_X: i32 = 50;
const MIN_Y: i32 = 30;

struct Model {
    field: Field,
    relm: Relm<Win>,
    // …
}
#[derive(Msg)]
enum Msg {
    // …
    Flip { x: usize, y: usize },
    NewRandomGame { size: usize },
    Solve,
    CleanField,
    Quit,
}
struct Win {
    // …
    model: Model,
    window: Window,
    vbox: gtk::Box,
    valid_col_labels: Vec<(usize, Label)>,
    valid_row_labels: Vec<(usize, Label)>,
    buttons: Vec<(usize, usize, Button)>,
}
impl Win {
    fn setup_ui(&mut self, relm: &Relm<Win>) {
        let window = &self.window;
        let model = &self.model;

        let vbox = &self.vbox;

        {
            let hbox = gtk::Box::new(Horizontal, 0);
            vbox.add(&hbox);
            for &size in &[4usize, 6, 7, 8, 10] {
                let restart_button = Button::with_label(&*format!("New Game {}x{}", size, size));
                hbox.add(&restart_button);
                connect!(
                    relm,
                    restart_button,
                    connect_clicked(_),
                    Msg::NewRandomGame { size }
                );
            }
            let solve_button = Button::with_label("Solve");
            hbox.add(&solve_button);
            connect!(relm, solve_button, connect_clicked(_), Msg::Solve);

            let empty_button = Button::with_label("Clean Field");
            hbox.add(&empty_button);
            connect!(relm, empty_button, connect_clicked(_), Msg::CleanField);
        }

        let mut valid_col_labels = vec![];
        let mut valid_row_labels = vec![];
        let mut buttons = vec![];

        let mut col_labels = || {
            let hbox = gtk::Box::new(Horizontal, 0);
            vbox.add(&hbox);

            // padding
            let label = Label::new(None);
            label.set_size_request(MIN_Y, MIN_Y);
            hbox.add(&label);

            for x in 0..model.field.width {
                let label = Label::new(Some("???"));
                label.set_size_request(MIN_X, MIN_Y);
                hbox.add(&label);
                valid_col_labels.push((x, label));
            }
        };

        col_labels();
        for y in 0..model.field.width {
            let hbox = gtk::Box::new(Horizontal, 0);
            vbox.add(&hbox);

            let mut row_label = || {
                let label = Label::new(Some("???"));
                label.set_size_request(MIN_Y, MIN_Y);
                hbox.add(&label);
                valid_row_labels.push((y, label));
            };

            row_label();
            for x in 0..model.field.width {
                let button = Button::with_label("...");
                connect!(relm, button, connect_clicked(_), Msg::Flip { x, y });
                button.set_size_request(MIN_X, MIN_Y);
                hbox.add(&button);
                buttons.push((x, y, button));
            }
            row_label();
        }
        col_labels();

        // Connect the signal `delete_event` to send the `Quit` message.
        connect!(
            relm,
            window,
            connect_delete_event(_, _),
            return (Some(Msg::Quit), Inhibit(false))
        );
        // There is also a `connect!()` macro for GTK+ events that do not need a value to be returned in the callback.

        window.show_all();

        self.valid_col_labels = valid_col_labels;
        self.valid_row_labels = valid_row_labels;
        self.buttons = buttons;

        self.update_ui();
    }
    fn update_ui(&self) {
        const RED: gdk::RGBA = gdk::RGBA {
            alpha: 1.0,
            red: 0.9,
            green: 0.0,
            blue: 0.0,
        };
        let update_label = |lb: &Label, valid| {
            let (s, color) = if valid {
                ("T", None)
            } else {
                ("f", Some(&RED))
            };
            lb.override_color(StateFlags::NORMAL, color);
            lb.set_label(s);
        };

        let field = &self.model.field;

        for yy in 0..field.width {
            for xx in 0..field.width {
                let fs = &field[[xx, yy]];
                let sealed = fs.is_sealed();
                let s = match fs {
                    FieldState::Unknown => " ",
                    FieldState::X => "x",
                    FieldState::Xs => "X",
                    FieldState::O => "o",
                    FieldState::Os => "O",
                };
                self.buttons
                    .iter()
                    .filter(|(x, y, _)| *x == xx && *y == yy)
                    .for_each(|(_, _, bt)| {
                        bt.set_label(s);
                        bt.set_sensitive(sealed == false);
                        // TODO farbe? dickere Schrift?
                    });
            }
            self.valid_row_labels
                .iter()
                .filter(|(y, _)| *y == yy)
                .for_each(|(y, lb)| {
                    update_label(lb, field.is_row_valid(*y));
                });
        }
        for xx in 0..field.width {
            self.valid_col_labels
                .iter()
                .filter(|(x, _)| *x == xx)
                .for_each(|(x, lb)| {
                    update_label(lb, field.is_col_valid(*x));
                });
        }

        let color = if self.model.field.is_complete() && self.model.field.is_valid() {
            gdk::RGBA {
                alpha: 1.0,
                red: 0.9,
                green: 1.0,
                blue: 0.9,
            }
        } else {
            gdk::RGBA {
                alpha: 1.0,
                red: 1.0,
                green: 1.0,
                blue: 0.9,
            }
        };
        self.window
            .override_background_color(StateFlags::NORMAL, Some(&color));
    }
}
impl Update for Win {
    // Specify the model used for this widget.
    type Model = Model;
    // Specify the model parameter used to init the model.
    type ModelParam = ();
    // Specify the type of the messages sent to the update function.
    type Msg = Msg;

    // Return the initial model.
    fn model(relm: &Relm<Self>, _params: ()) -> Model {
        let field = "X|O| |O\n\
                     O| | | \n\
                     X| | |O\n\
                     X|O| |O\n"
            .parse()
            .unwrap();
        Model {
            field,
            relm: relm.clone(),
        }
    }

    // The model may be updated when a message is received.
    // Widgets may also be updated in this function.
    fn update(&mut self, event: Msg) {
        match event {
            Msg::Quit => gtk::main_quit(),
            Msg::NewRandomGame { size } => {
                for child in self.vbox.get_children() {
                    self.vbox.remove(&child);
                }
                self.model.field = loop {
                    let mut f = Field::new_random(size);
                    if f.is_valid() {
                        f.seal();
                        break f;
                    }
                };
                self.setup_ui(&self.model.relm.clone());
                self.update_ui();
            }
            Msg::Flip { x, y } => {
                self.model.field[[x, y]] = match self.model.field[[x, y]] {
                    FieldState::Unknown | FieldState::X => FieldState::O,
                    FieldState::O => FieldState::X,
                    FieldState::Os | FieldState::Xs => {
                        unreachable!("must never try to change sealed field ({}/{})", x, y)
                    }
                };
                self.update_ui();
            }
            Msg::Solve => {
                let result = self.model.field.solve();
                println!("{:?}", result);
                if let Ok(field) = result {
                    self.model.field = field;
                }
                self.update_ui();
            }
            Msg::CleanField => {
                self.model.field.clear();
                self.update_ui();
            }
        }
    }
}
impl UpdateNew for Win {
    fn new(_relm: &Relm<Self>, _model: Model) -> Self {
        todo!("UpdateNew()")
    }
}

impl Widget for Win {
    // Specify the type of the root widget.
    type Root = Window;

    // Return the root widget.
    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    // Create the widgets.
    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        // GTK+ widgets are used normally within a `Widget`.
        let window = Window::new(WindowType::Toplevel);

        let vbox = gtk::Box::new(Vertical, 0);
        window.add(&vbox);

        let mut win = Win {
            model,
            window,
            vbox,
            valid_col_labels: vec![],
            valid_row_labels: vec![],
            buttons: vec![],
        };
        win.setup_ui(relm);
        win
    }
}

fn main() {
    Win::run(()).unwrap();
}
